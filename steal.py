#!/usr/bin/env python3
import sys, requests, zipfile

def getId(url, page=False):
    parts = url.split("/")
    start = parts.index("ark:")
    result = tuple(parts[(start+1):start+3])
    if page:
        return result + (parts[start+3].split(".")[0],)
    else:
        return result

def getPages(id):
    url = "https://gallica.bnf.fr/services/ajax/pagination/SINGLE/ark:/{}/{}/f2.item".format(*id)
    result = requests.get(url).json()
    for content in result["fragment"]["contenu"]:
        yield (content["contenu"], getId(content["url"], page=True))

def pageName(index, page):
    return "{:04}.page-{}.jpg".format(index, page[0])

def pageUrl(page):
    return "https://gallica.bnf.fr/ark:/{}/{}/{}.highres".format(*page[1])

if __name__ == "__main__":
    url = ""
    try:
        url = sys.argv[1]
    except IndexError:
        print("Usage: {} <URL>\n  Steal books from https://gallica.bnf.fr/".format(sys.argv[0]))
        sys.exit(1)
    id = getId(url, page=False)
    filename = "book.{}.{}.zip".format(*id)
    i = 0
    pages = list(getPages(id))
    with zipfile.ZipFile(filename, mode="w", compression=zipfile.ZIP_DEFLATED, compresslevel=9) as archive:
        for page in pages:
            i += 1
            with archive.open(pageName(i, page), "w") as f:
                print("{}/{}".format(i, len(pages)), pageName(i, page), pageUrl(page), file=sys.stderr)
                f.write(requests.get(pageUrl(page)).content)
    print(filename)
